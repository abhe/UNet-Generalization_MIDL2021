''' Understanding and Visualizing Generalization in UNets
    Authors:    Abhejit Rajagopal <abhejit.rajagopal@ucsf.edu>
                Vamshi C. Madala <vamshichowdary@ucsb.edu>
'''
import os, sys, time, random, itertools, glob, pdb
from tqdm import tqdm
import numpy as np
import pandas as pd
from collections import OrderedDict, defaultdict
import h5py
from pprint import pprint
import torch
import pickle
import scipy.ndimage
import skimage
import sklearn
import sklearn.decomposition
import sklearn.cluster
import sklearn.mixture

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.colors as colors

import pseg, datamodel
from mltools import torchutils

_USER_ = ''

def load_data(reader, reader_kwargs, batchsize=3, datapath=None):
	def load_catalog(tidx, volsize,batchsize, datapath=None):
		catalog = torchutils.hdfstore2dict(tidx, '', debug=1)['UCSF_fusion']
		if datapath is not None:
			for ki,(k,v) in tqdm(enumerate(catalog.iterrows()), total=catalog.shape[0]):
				# print(ki, k, v, flush=True)
				catalog.loc[k, 'file'] = os.path.join(datapath,catalog.loc[k, 'file'].split('/')[-1])
			#
		#
		cut_low = 10
		cut_mid = np.product(volsize)*0.15
		catalog_grouped = {'null':catalog[catalog['pix_valid']<=cut_low], 'gland_border':catalog[(catalog['pix_valid']>cut_low) & (catalog['pix_valid']<cut_mid)], 'gland_main':catalog[(catalog['pix_valid']>=cut_mid)]}
		ds_train, dl_train, count_train = torchutils.load_traindata_groups(catalog_grouped, reader, resample='min', batchsize=batchsize, num_workers=24, pin_memory=True, print_groups=True, mode='idx', **reader_kwargs)
		return dl_train
	#
	volsize = reader_kwargs['volsize']
	trainidx = '/scratch/{}/prostate/seg_trainidx_{}.H5'.format(_USER_, '-'.join(['{}'.format(k) for k in volsize]))
	validx = '/scratch/{}/prostate/seg_tvalidx_{}.H5'.format(_USER_, '-'.join(['{}'.format(k) for k in volsize]))
	dl_train = load_catalog(trainidx, volsize, batchsize, datapath=datapath)
	dl_val = load_catalog(validx, volsize, batchsize, datapath=datapath)
	return dl_train, dl_val
##

def get_batches(dataloader, num_batches=2):
	batches = []
	for si,s in tqdm(enumerate(dataloader), disable=True):
		if si >= num_batches: break
		s['MR'] = s['MR'].to("cuda")
		gland_predicted, interim_features = model(s['MR'], get_encoded='all', training=True)
		gland_predicted = gland_predicted.to("cpu").detach().numpy()
		interim_features, interim_weightnorms = zip(*interim_features)
		interim_features = [k.to("cpu").detach().numpy() for k in interim_features]
		interim_weightnorms = np.stack([k.to("cpu").detach().numpy() for k in interim_weightnorms],axis=0)
		# pdb.set_trace()
		gland_mask = s['gland_mask'].to("cpu").detach().numpy()
		MR = s['MR'].to("cpu").detach().numpy()
		IoU = torchutils.IoU_nhot(gland_predicted, gland_mask, _numpy=True).squeeze()
		batches.append([MR, gland_mask, gland_predicted, interim_features, interim_weightnorms, IoU])
	#
	return batches
##

def collate_batches(batches):
	MR, gland_mask, gland_predicted, interim_features, interim_weightnorms, IoU = zip(*batches)
	MR = np.concatenate(MR,axis=0)
	gland_mask = np.concatenate(gland_mask,axis=0)
	gland_predicted = np.concatenate(gland_predicted,axis=0)
	interim_weightnorms = np.stack(interim_weightnorms,axis=1).mean(axis=1)
	interim_features = [np.concatenate(k,axis=0) for k in zip(*interim_features)]
	IoU = np.concatenate(IoU,axis=0)
	all_feats = [MR, *interim_features, gland_predicted]
	[k.shape for k in all_feats]
	return [all_feats, gland_mask, interim_weightnorms, IoU]
##

def downsample_to_layer(target, src, quantize=True, order=3):
	## DOWNSAMPLE GROUNDTRUTH TO LAYER'S FEATURE SIZE
	zoom_factor = list(np.asarray(target.shape[-3:])/np.asarray(src.shape[-3:]))
	src_downsampled = scipy.ndimage.zoom(src, [1,1,*zoom_factor], order=order, mode='constant', cval=0.0,)
	assert(src_downsampled.shape[-3:]==target.shape[-3:])
	src_quantized = np.ceil(src_downsampled)
	# print(target.shape, src_downsampled.mean(), src_quantized.mean())
	return src_quantized
##

def make_receptive(interim, kernel=[3,3,3], stride=[2,2,2], makeflat=True):
	features = np.pad(interim, ((0, 0), (1,1), (1,1),(1,1), (0,0)), 'constant')
	# pdb.set_trace()
	windows = skimage.util.view_as_windows(features, window_shape=[1,*kernel,interim.shape[-1]], step=[1,*stride,interim.shape[-1]]).squeeze((4,5))
	windows = windows.reshape([*windows.shape[0:4], np.product(windows.shape[4:7]), windows.shape[-1] ] )
	if makeflat: windows = windows.reshape([windows.shape[0], np.product(windows.shape[1:4]), *windows.shape[-2:] ] )
	return windows
##

def align_and_subsample(paired_data, l, MAX_SAMPLES=60000):
	## ALIGN AND RESAMPLE (USING RECEPTIVE FIELD)

	## INPUTS/OUTPUTS [batch, X, Y, Z, channels]
	# vecs_t = [vec.transpose((0,2,3,4,1)) for vec in vecs]
	src = paired_data[l-1]['feat'].transpose((0,2,3,4,1))
	src_gt = paired_data[l-1]['mask'].transpose((0,2,3,4,1))
	dst_predicted = paired_data[l]['feat'].transpose((0,2,3,4,1))
	dst_gt = paired_data[l]['mask'].transpose((0,2,3,4,1))

	## ALIGN DATA USING RECEPTIVE FIELD
	pt_src = make_receptive(src, kernel=[3,3,3], stride=[2,2,2], makeflat=True)
	pt_src = pt_src.reshape([*pt_src.shape[0:2],-1])
	pt_src_gt = make_receptive(src_gt, kernel=[3,3,3], stride=[2,2,2], makeflat=True).max(axis=(2))
	pt_dst_pred = dst_predicted.reshape([dst_predicted.shape[0],np.product(dst_predicted.shape[1:4]),-1])
	pt_dst_gt = dst_gt.reshape([dst_gt.shape[0],np.product(dst_gt.shape[1:4]),-1])

	## SUB-SAMPLE DATA
	# MAX_SAMPLES = 60000
	num_per_example = MAX_SAMPLES // pt_src.shape[0]
	sample_idx = []
	for b in range(pt_src.shape[0]):
		uniq = np.unique(pt_src_gt[b])
		# print(uniq)
		num_per_class = num_per_example // len(uniq)
		for lab in uniq:
			labmask = np.argwhere(pt_src_gt[b]==lab)[:,0]
			sampsidx = np.random.choice(labmask, size=min(num_per_class,labmask.shape[0]))
			sampsidx = np.stack( [np.repeat(b,len(sampsidx)), sampsidx], axis=0).T
			sample_idx.append(sampsidx)
		#
	#
	sample_idx = np.concatenate(sample_idx,axis=0)
	pt_src = pt_src[sample_idx[:,0],sample_idx[:,1]]
	pt_src_gt = pt_src_gt[sample_idx[:,0],sample_idx[:,1]]
	pt_dst_pred = pt_dst_pred[sample_idx[:,0],sample_idx[:,1]]
	pt_dst_gt = pt_dst_gt[sample_idx[:,0],sample_idx[:,1]]

	return [pt_src, pt_src_gt, pt_dst_pred, pt_dst_gt]
##

def prep_wholesymbol(paired_data, l, MAX_SAMPLES=60000):
	## INPUTS/OUTPUTS [batch, X, Y, Z, channels]
	src = paired_data[l-1]['feat'][0:MAX_SAMPLES].transpose((0,2,3,4,1))
	src_gt = paired_data[l-1]['mask'][0:MAX_SAMPLES].transpose((0,2,3,4,1))
	dst_predicted = paired_data[l]['feat'][0:MAX_SAMPLES].transpose((0,2,3,4,1))
	dst_gt = paired_data[l]['mask'][0:MAX_SAMPLES].transpose((0,2,3,4,1))
	# pdb.set_trace()

	# simply vectorize src/dst symbols
	pt_src = src.reshape([src.shape[0],-1])
	pt_dst_pred = dst_predicted.reshape([dst_predicted.shape[0],-1])

	# GT labels is just the mean.
	pt_src_gt = (src_gt>0).mean(axis=(1,2,3,4))
	pt_src_gt = (1.0*(pt_src_gt==0) + 1.0*( (pt_src_gt>0)&(pt_src_gt<0.15) ) + 2.0*(pt_src_gt>0.15))[:,None]
	pt_dst_gt = (dst_gt>0).mean(axis=(1,2,3,4))
	pt_dst_gt = (1.0*(pt_dst_gt==0) + 1.0*( (pt_dst_gt>0)&(pt_dst_gt<0.15) ) + 2.0*(pt_dst_gt>0.15))[:,None]

	return [pt_src, pt_src_gt, pt_dst_pred, pt_dst_gt]
##

def learn_manifold(pt_src, pt_dst_pred, src_PCA_components=5, dst_PCA_components=5, src_kmeans_components=5, dst_kmeans_components=5):
	# break
	# n_components = 30
	# n_components = 'mle'
	pca_src = sklearn.decomposition.PCA(min(src_PCA_components,pt_src.shape[-1]))
	# pca_src = sklearn.decomposition.SparsePCA(n_components)
	# pca_src = sklearn.decomposition.KernelPCA(n_components, degree='rbf')
	pt_src_transformed = pca_src.fit_transform(pt_src)
	kmeans_src = sklearn.cluster.KMeans(n_clusters=min(src_kmeans_components,pt_src_transformed.shape[0]//30))
	clustidx_src = kmeans_src.fit_predict(pt_src_transformed)
	# clustidx_src = group_labels = kmeans_src.labels_
	cluster_masks_src = [(clustidx_src==k).squeeze() for k in np.unique(clustidx_src)]
	mixtures_src = [sklearn.mixture.GaussianMixture(n_components=min(3,mask.sum())).fit(pt_src_transformed[mask]) for mask in cluster_masks_src]

	pca_dst = sklearn.decomposition.PCA(min(dst_PCA_components,pt_dst_pred.shape[1]))
	# pca_dst = sklearn.decomposition.SparsePCA(n_components)
	# pca_dst = sklearn.decomposition.KernelPCA(n_components, degree='rbf')
	pt_dst_transformed = pca_dst.fit_transform(pt_dst_pred)
	kmeans_dst = sklearn.cluster.KMeans(n_clusters=min(dst_kmeans_components,pt_dst_transformed.shape[0]//30))
	clustidx_dst = kmeans_dst.fit_predict(pt_dst_transformed)
	cluster_masks_dst = [(clustidx_dst==k).squeeze() for k in np.unique(clustidx_dst)]
	mixtures_dst = [sklearn.mixture.GaussianMixture(n_components=min(3,mask.sum())).fit(pt_dst_transformed[mask]) for mask in cluster_masks_dst]

	return [(pca_src, kmeans_src, mixtures_src), (pca_dst, kmeans_dst, mixtures_dst)]
##

def project_manifold(pts, manifolds):
	pt_src, pt_dst_pred = pts
	(pca_src, kmeans_src, mixtures_src), (pca_dst, kmeans_dst, mixtures_dst) = manifolds
	pt_src_transformed = pca_src.transform(pt_src)
	pt_dst_transformed = pca_dst.transform(pt_dst_pred)
	src_clustidx = kmeans_src.predict(pt_src_transformed)
	dst_clustidx = kmeans_dst.predict(pt_dst_transformed)
	src_gmm_confidence = np.stack([m.score_samples(pt_src_transformed) for m in mixtures_src],axis=1)
	dst_gmm_confidence = np.stack([m.score_samples(pt_dst_transformed) for m in mixtures_dst],axis=1)
	return (pt_src_transformed, src_clustidx, src_gmm_confidence), (pt_dst_transformed, dst_clustidx, dst_gmm_confidence)
##

def compute_db(pt_data, cluster_masks, idx=True):
	if idx:
		cluster_idx = cluster_masks
	else:
		cluster_idx = np.stack(cluster_masks,axis=1).argmax(axis=1)
	#
	return sklearn.metrics.davies_bouldin_score(pt_data, cluster_idx)
##

def compute_confidence(mixtures, kmeans, pts):
	mixtures_src, mixtures_dst, = mixtures
	kmeans_src, kmeans_dst = kmeans
	pt_src_transformed2, pt_dst_transformed2 = pts
	scores = np.stack([m.score_samples(pt_src_transformed2) for m in mixtures_src],axis=1)
	scores = scipy.special.softmax(scores, axis=1).max(axis=1)
	score_weight = np.linalg.norm(pt_dst_transformed2,axis=1)
	return scores, score_weight
##

def compute_metrics(src_data, dst_data):
	(pt_src, pt_src_gt, pt_src_transformed, src_clustidx, src_gmm_confidence, mixtures_src, kmeans_src) = src_data
	(pt_dst_pred, pt_dst_gt, pt_dst_transformed, dst_clustidx, dst_gmm_confidence, mixtures_dst, kmeans_dst) = dst_data

	## COMPUTE CLUSTERING METRIC USING PT_COR (LOCAL MASK), AVG FOR EACH "NEURON" --> This is a new DB
	db_gtval_src = compute_db(pt_src_transformed, pt_dst_gt.squeeze())

	## COMPUTE CLUSTERING METRIC USING PT_TARG (NEXT MASK) --> This is closer to the orig DB
	db_gtval_dst = compute_db(pt_src_transformed, pt_dst_gt.squeeze())

	## COMPUTE CLUSTERING USING PT_PRED --> This is the roughness metric
	db_roughness = compute_db(pt_src_transformed, dst_clustidx)

	## COMPUTE CONFIDENCE -->
	scores, score_weight = compute_confidence([mixtures_src, mixtures_dst], [kmeans_src, kmeans_dst], [pt_src_transformed,pt_dst_transformed])
	wscores = scores * score_weight
	conf = scores.mean()
	wconf = wscores.mean()

	metrics = {'DB_src':db_gtval_src,'DB_dst':db_gtval_dst,'DB_roughness':db_roughness,'conf':conf,'wconf':wconf}
	return metrics
##

def scatterplot_metrics(save_metrics):
	overall = defaultdict(list)
	for metrics in save_metrics:
		metrics = pd.DataFrame(metrics)
		for k in sorted(pd.DataFrame(metrics).keys()):
			if 'layer' in k: continue
			if 'weightnorm' in k: continue
			y = metrics['test_IoU'].mean()
			if not np.isfinite(y):
				pdb.set_trace()
				continue
				y = metrics['test_sample_IoU'].mean()
			#
			x = np.asarray(metrics[k])
			if 'IoU' not in k:
				x = x / np.asarray(metrics['weightnorm'])
			#
			x = x.mean()
			if not np.isfinite(x):
				pdb.set_trace()
				continue
			#
			assert(np.isfinite(x) and np.isfinite(y))
			overall[k].append( (x, y) )
		#
	#

	cmap_plot = 'jet'
	ncols = len(overall.keys())//3 
	nrows = int(np.ceil(len(overall.keys()) / ncols))
	fig, axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=(4*ncols, 4*nrows))
	axs = axs.ravel()
	for ki, (k,v) in enumerate(overall.items()):
		ax = axs[ki]
		vals, ious = zip(*v)
		Rsquared =  np.corrcoef(vals, ious)[0,1]
		Rsqured_txt = '$\\rho$={:.3f}'.format(Rsquared)
		ax.scatter(vals, ious, c=ious, cmap=cmap_plot, vmin=0, vmax=1, edgecolors='black')
		ax.set_xlabel(k +', '+Rsqured_txt)
		ax.set_ylabel('Test IoU')
	#
	fig.tight_layout()
	fig.savefig('val_scatter.png', dpi=200)
	# pdb.set_trace()
	plt.close('all')
	del fig
##

def lineplot_metrics(save_metrics):
	overall = defaultdict(list)
	for metrics in save_metrics:
		metrics = pd.DataFrame(metrics)#[0:6]
		for k in sorted(pd.DataFrame(metrics).keys()):
			if 'layer' in k: continue
			if 'weightnorm' in k: continue
			if 'IoU' in k: continue
			y = metrics['test_IoU'].mean()
			x = np.asarray(metrics[k])
			if 'IoU' not in k:
				x = x / np.asarray(metrics['weightnorm'])
			#
			if not (np.any(np.isfinite(x)) or np.any(np.isfinite(y))):
				pdb.set_trace()
				continue
			#
			z = np.asarray(metrics['layer'])
			overall[k].append( (z, x, y) )
		#
	#

	cmap_plot = plt.cm.jet
	clr_norm = colors.Normalize(0.00, 1.00)
	ncols = len(overall.keys())//3 
	nrows = int(np.ceil(len(overall.keys()) / ncols))
	fig, axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=(4*ncols, 4*nrows))
	axs = axs.ravel()
	for ki, (k,v) in enumerate(overall.items()):
		layerNum, vals, ious = zip(*v)
		vals = np.asarray(vals)
		layerNum = np.asarray(layerNum)
		clrs = cmap_plot(clr_norm(ious))
		ax = axs[ki]
		for p in range(layerNum.shape[0]):
			ax.plot(layerNum[p], vals[p], c=clrs[p])
		#
		ax.set_xlabel('Layer #')
		ax.set_ylabel(k)
	#
	fig.tight_layout()
	fig.savefig('val_lineplot.png', dpi=200)
	plt.close('all')
	del fig
##


if __name__ == '__main__':
	_LOSS_all = ['MAE', 'BCE-balanced', 'DICE', 'DICE-BCE']
	f_maps = 32
	numGPUs = 1
	num_workers = 38
	traintest = './data/traintest.npz'
    modeldir_base = '/scratch/{}/prostate/pseg-models/'.format(_USER_)
	reader = datamodel.read_UCSF_T2Seg
	test_reader_kwargs = {'MR_key':'MR', 'Seg_key':'gland_mask', 'volsize':[64,64,16], 'stride':[48,48,12], 'randomize':False,'flip':False,'doubletrouble':False,'debug':0}
	test_batchsize = 20
	train_files = np.load(traintest)['train_files']
	val_files = np.load(traintest)['val_files']

	overwrite_eval = False
	overwrite_eval2 = False
	results_pkls = './results/pkls/'
	results_metrics = './results/metrkic-pkls/'
	os.makedirs(results_pkls, exist_ok=True)
	os.makedirs(results_metrics, exist_ok=True)
	print_verbose = False

	all_models = [k for k in sorted(glob.glob(modeldir_base+'*/*.ckpt'))[::-1] if 'last' not in k]
	ALL_RESULTS = []
	for checkpoint in tqdm(all_models):
		_LOSS_ = checkpoint.split('/')[-2]
		fbase = checkpoint.split('/')[-1].split('.ckpt')[0]
		epoch = fbase.split('epoch=')[-1].split('-')[0]
		save_pkl = os.path.join(results_pkls, '{}.pkl'.format(fbase))
		if overwrite_eval or not os.path.exists(save_pkl):
			model = pseg.load_model(checkpoint, loss=_LOSS_, f_maps=f_maps)
			train_metrics = pseg.evaluate(train_files[0:len(val_files)], model, test_batchsize, overwrite=True, savedir_h5=None, savedir_png=None, reader=reader, reader_kwargs=test_reader_kwargs, num_workers=num_workers, debug=print_verbose)
			val_metrics = pseg.evaluate(val_files, model, test_batchsize, overwrite=True, savedir_h5=None, savedir_png=None, reader=reader, reader_kwargs=test_reader_kwargs, num_workers=num_workers, debug=print_verbose)
			train_metrics['loss'] = _LOSS_
			train_metrics['epoch'] = int(epoch)
			val_metrics['loss'] = _LOSS_
			val_metrics['epoch'] = int(epoch)
			pickle.dump([train_metrics, val_metrics], open(save_pkl,'wb'))
		#
		[train_metrics, val_metrics] = pickle.load(open(save_pkl,'rb'))
		if len(ALL_RESULTS)==0:
			to_append_file = {'file': [k.split('/')[0] for k in val_metrics['file']]}
			ALL_RESULTS.append(to_append_file)
		#
		codename = '{obj}-{epoch}'.format(obj=_LOSS_, epoch=epoch)
		to_append = {codename: val_metrics['IoU']}
		ALL_RESULTS.append(to_append)
	#
	ALL_RESULTS = {k:v for ckpt in ALL_RESULTS for k,v in ckpt.items()}
	ALL_RESULTS = pd.DataFrame(ALL_RESULTS)

	fig, axs = plt.subplots(1,1, figsize=(4,4))
	markers = ['o', 'X', 'd' , '*']
	cmap_plot = 'jet'
	for ki,(k,v) in enumerate(sorted(ALL_RESULTS.iteritems())):
		if 'file' in k: continue
		epoch = k.split('-')[-1]
		_LOSS_ = '-'.join(k.split('-')[:-1])
		marker_idx = int(np.argwhere([_LOSS_==k for k in _LOSS_all]).squeeze())
		epoch = int(k.split('-')[-1])
		axs.scatter(epoch, v.mean(), c=v.mean(), cmap=cmap_plot, vmin=0, vmax=1, marker=markers[marker_idx], edgecolors='black') # color=error/accruacy
	#
	axs.set_xlabel('Epoch')
	axs.set_ylabel('Test IoU')
	axs.legend(loc='best', numpoints=1)
	fig.tight_layout()
	fig.savefig('./val_models.png', dpi=300)

	fig, axs = plt.subplots(1,1, figsize=(4,4))
	for ki,(k,v) in enumerate(sorted(ALL_RESULTS.iterrows())):
		axs.plot(np.asarray(v[1:]))
	#
	fig.savefig('./val_PERITEM.png', dpi=200)

	test_reader_kwargs2 = {'MR_key':'MR', 'Seg_key':'gland_mask', 'volsize':[64,64,16], 'stride':[64,64,16], 'randomize':False,'flip':False,'doubletrouble':False,'debug':0}
	test_batchsize2 = 30
	num_batches = 50
	MAXSAMPLES_PCA_TRAIN = 80000
	NUM2PLOT = 1000
	MAXSAMPLES_PCA_EVAL = MAXSAMPLES_PCA_TRAIN*2
	downsample_order = 0
	sample_checkpoints = all_models
	save_metrics = []
	for checkpoint in tqdm(sample_checkpoints):
		_LOSS_ = checkpoint.split('/')[-2]
		fbase = checkpoint.split('/')[-1].split('.ckpt')[0]
		epoch = fbase.split('epoch=')[-1].split('-')[0]
		load_pkl = os.path.join(results_pkls, '{}.pkl'.format(fbase))
		save_pkl = os.path.join(results_metrics, '{}.pkl'.format(fbase))
		if os.path.exists(save_pkl) and not overwrite_eval2:
			metrics = pickle.load(open(save_pkl,'rb'))
			metrics_pd = pd.DataFrame(metrics)
			metrics = pickle.load(open(save_pkl,'rb'))
			metrics_pd = pd.DataFrame(metrics)
			assert('weightnorm' in metrics_pd.keys())
			save_metrics.append(metrics)
			continue
		#
		if os.path.exists(load_pkl):
			[train_metrics, val_metrics] = pickle.load(open(load_pkl,'rb'))
			train_IoU = train_metrics['IoU'].mean()
			test_IoU = val_metrics['IoU'].mean()
		else:
			continue
			train_IoU = np.NaN
			test_IoU = np.NaN
		#

		metrics = []
		print('==>', _LOSS_, fbase, save_pkl)
		model = pseg.load_model(checkpoint, loss=_LOSS_, f_maps=32)
		model = model.to("cuda")

		dl_train, dl_val = load_data(reader, test_reader_kwargs2, batchsize=test_batchsize2)

		train_batches = get_batches(dl_train, num_batches=num_batches)
		val_batches = get_batches(dl_val, num_batches=num_batches)

		[all_feats_train, gland_mask_train, model_weightnorms, IoU_train] = collate_batches(train_batches[:-5])
		[all_feats_val, gland_mask_val, _, IoU_val] = collate_batches(train_batches[-5:])
		[all_feats_test, gland_mask_test, _, IoU_test] = collate_batches(val_batches)

		paired_data_train = [{} for l in range(len(all_feats_train))]
		paired_data_val = [{} for l in range(len(all_feats_val))]
		paired_data_test = [{} for l in range(len(all_feats_test))]
		# pdb.set_trace()
		wIdx = 0
		for lyrNum in range(len(all_feats_train)):
			## DOWNSAMPLE GROUNDTRUTH TO LAYER'S FEATURE SIZE
			paired_data_train[lyrNum]['feat'] = all_feats_train[lyrNum]
			paired_data_train[lyrNum]['mask'] = gland_mask_train
			print('\t', lyrNum, paired_data_train[lyrNum]['feat'].shape, 'local {:3f} <--> {:3f}'.format(paired_data_train[lyrNum]['mask'].mean(), gland_mask_train.mean()) ,flush=True)

			paired_data_val[lyrNum]['feat'] = all_feats_val[lyrNum]
			paired_data_val[lyrNum]['mask'] = gland_mask_val
			print('\t\t', paired_data_val[lyrNum]['feat'].shape, 'local {:3f} <--> {:3f}'.format(paired_data_val[lyrNum]['mask'].mean(), gland_mask_val.mean()) ,flush=True)

			paired_data_test[lyrNum]['feat'] = all_feats_test[lyrNum]
			paired_data_test[lyrNum]['mask'] = gland_mask_test
			print('\t\t', paired_data_test[lyrNum]['feat'].shape, 'local {:3f} <--> {:3f}'.format(paired_data_test[lyrNum]['mask'].mean(), gland_mask_test.mean()) ,flush=True)
			if lyrNum<=1: continue
			# pdb.set_trace()

			## (OLD) TRAIN MANIFOLD
			[old_train_src, old_train_src_gt, old_train_dst_pred, old_train_dst_gt] = prep_wholesymbol(paired_data_train, lyrNum, MAX_SAMPLES=MAXSAMPLES_PCA_TRAIN)
			print('\t\t --> PCA!',flush=True)
			[(old_pca_src, old_kmeans_src, old_mixtures_src), (old_pca_dst, old_kmeans_dst, old_mixtures_dst)] = old_train_manifolds = learn_manifold(old_train_src, old_train_dst_pred) 

			## (OLD) PROJECT MANIFOLD
			print('\t\t --> Eval Train!',flush=True)
			old_train_src_transformed, old_train_dst_transformed = project_manifold( (old_train_src, old_train_dst_pred), old_train_manifolds) # NOTICE, NO GT HERE! :)
			old_layer_metrics_train = compute_metrics([old_train_src, old_train_src_gt, *old_train_src_transformed, old_mixtures_src, old_kmeans_src], [old_train_dst_pred, old_train_dst_gt, *old_train_dst_transformed, old_mixtures_dst, old_kmeans_dst])

			print('\t\t --> Eval Val!',flush=True)
			[old_val_src, old_val_src_gt, old_val_dst_pred, old_val_dst_gt] = prep_wholesymbol(paired_data_val, lyrNum, MAX_SAMPLES=MAXSAMPLES_PCA_EVAL)
			old_val_src_transformed, old_val_dst_transformed = project_manifold( (old_val_src, old_val_dst_pred), old_train_manifolds) # NOTICE, NO GT HERE! :)
			old_layer_metrics_val = compute_metrics([old_val_src, old_val_src_gt, *old_val_src_transformed, old_mixtures_src, old_kmeans_src], [old_val_dst_pred, old_val_dst_gt, *old_val_dst_transformed, old_mixtures_dst, old_kmeans_dst])

			print('\t\t --> Eval Test!',flush=True)
			[old_test_src, old_test_src_gt, old_test_dst_pred, old_test_dst_gt] = prep_wholesymbol(paired_data_test, lyrNum, MAX_SAMPLES=MAXSAMPLES_PCA_EVAL)
			old_test_src_transformed, old_test_dst_transformed = project_manifold( (old_test_src, old_test_dst_pred), old_train_manifolds) # NOTICE, NO GT HERE! :)
			old_layer_metrics_test = compute_metrics([old_test_src, old_test_src_gt, *old_test_src_transformed, old_mixtures_src, old_kmeans_src], [old_test_dst_pred, old_test_dst_gt, *old_test_dst_transformed, old_mixtures_dst, old_kmeans_dst])

			## NEW TRAIN MANIFOLD
			print('\t\t --> PCA!',flush=True)
			[train_src, train_src_gt, train_dst_pred, train_dst_gt] = align_and_subsample(paired_data_train, lyrNum, MAX_SAMPLES=MAXSAMPLES_PCA_TRAIN)
			[(pca_src, kmeans_src, mixtures_src), (pca_dst, kmeans_dst, mixtures_dst)] = train_manifolds = learn_manifold(train_src, train_dst_pred) # NOTICE, NO GT HERE! :)

			## PROJECT/EVAL MANIFOLD
			[train_src, train_src_gt, train_dst_pred, train_dst_gt] = align_and_subsample(paired_data_train, lyrNum, MAX_SAMPLES=MAXSAMPLES_PCA_EVAL)
			train_src_transformed, train_dst_transformed = project_manifold( (train_src, train_dst_pred), train_manifolds) # NOTICE, NO GT HERE! :)
			layer_metrics_train = compute_metrics([train_src, train_src_gt, *train_src_transformed, mixtures_src, kmeans_src], [train_dst_pred, train_dst_gt, *train_dst_transformed, mixtures_dst, kmeans_dst])

			[val_src, val_src_gt, val_dst_pred, val_dst_gt] = align_and_subsample(paired_data_val, lyrNum, MAX_SAMPLES=MAXSAMPLES_PCA_EVAL)
			val_src_transformed, val_dst_transformed = project_manifold( (val_src, val_dst_pred), train_manifolds) # NOTICE, NO GT HERE! :)
			layer_metrics_val = compute_metrics([val_src, val_src_gt, *val_src_transformed, mixtures_src, kmeans_src], [val_dst_pred, val_dst_gt, *val_dst_transformed, mixtures_dst, kmeans_dst])

			[test_src, test_src_gt, test_dst_pred, test_dst_gt] = align_and_subsample(paired_data_test, lyrNum, MAX_SAMPLES=MAXSAMPLES_PCA_EVAL)
			test_src_transformed, test_dst_transformed = project_manifold( (test_src, test_dst_pred), train_manifolds) # NOTICE, NO GT HERE! :)
			layer_metrics_test = compute_metrics([test_src, test_src_gt, *test_src_transformed, mixtures_src, kmeans_src], [test_dst_pred, test_dst_gt, *test_dst_transformed, mixtures_dst, kmeans_dst])

			layer_metrics = {'layer':lyrNum}
			for k,v in layer_metrics_train.items(): layer_metrics['train_'+k] = v
			layer_metrics['train_sample_IoUs'] = IoU_train
			layer_metrics['train_sample_IoU'] = IoU_train.mean()
			layer_metrics['train_IoU'] = train_IoU

			for k,v in layer_metrics_val.items(): layer_metrics['val_'+k] = v
			layer_metrics['val_sample_IoUs'] = IoU_val
			layer_metrics['val_sample_IoU'] = IoU_val.mean()
			layer_metrics['val_IoU'] = train_IoU # NOTE: same as train

			for k,v in layer_metrics_test.items(): layer_metrics['test_'+k] = v
			layer_metrics['test_sample_IoUs'] = IoU_test
			layer_metrics['test_sample_IoU'] = IoU_test.mean()
			layer_metrics['test_IoU'] = test_IoU

            for k,v in old_layer_metrics_train.items(): layer_metrics['train_'+k+'_FullSymbol2'] = v
			for k,v in old_layer_metrics_val.items(): layer_metrics['val_'+k+'_FullSymbol2'] = v
			for k,v in old_layer_metrics_test.items(): layer_metrics['test_'+k+'_FullSymbol2'] = v

			layer_metrics['weightnorm'] = model_weightnorms[wIdx]
			wIdx += 1

			metrics.append(layer_metrics)
		#
		pickle.dump(metrics, open(save_pkl,'wb'))
		metrics_pd = pd.DataFrame(metrics)
		print(metrics_pd)
		save_metrics.append(metrics)
		scatterplot_metrics(save_metrics)
		lineplot_metrics(save_metrics)
	#
	scatterplot_metrics(save_metrics)
	lineplot_metrics(save_metrics)
##
